provider "aws" {
  region                  = "eu-central-1"
  shared_credentials_file = "/home/kosmas/.aws/credentials"
}

resource "aws_instance" "example" {
  ami           = "ami-090f10efc254eaf55"
  instance_type = "t2.micro"

  tags = {
    Name = "terraform-example"
  }
}
